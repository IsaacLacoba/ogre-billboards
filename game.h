#ifndef GAME_HPP
#define GAME_HPP
#include <memory>

#include "scene.h"
#include "input.h"
#include "timer.h"

class Game {
  const float FPS = 60;
  Timer _timer;

  void add_hooks();
  void init_scene();

 public:
  typedef std::shared_ptr<Game> shared;

  float _delta;

  Scene::shared _scene;
  EventListener::shared _input;

  Game();
  virtual ~Game();

  void shutdown();
  void start();

 private:
  void game_loop();
};

#endif
