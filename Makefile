DIRECTORIES= managers utils

LOCALHEADERFLAGS=$(foreach directory, . $(DIRECTORIES), -I$(directory))

OBJECTS = game.o scene.o input.o timer.o

vpath %.cpp $(DIRECTORIES)


CXX=g++ -std=c++11

CXXFLAGS = -Wall -ggdb $(LOCALHEADERFLAGS) $(shell pkg-config --cflags OGRE OIS)

LDLIBS = -lboost_system $(shell pkg-config --libs-only-l OGRE OIS)

all: main

main: ${@:%.o} $(OBJECTS)

clean:
	$(RM) main *.o *~ *# *.orig config/ogre.cfg config/ogre.log
