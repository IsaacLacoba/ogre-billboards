#include "game.h"

Game::Game() {
  _scene = std::make_shared<Scene>();
  _input = std::make_shared<EventListener>(_scene->_window);
}

Game::~Game() {
}

void
Game::start() {
  add_hooks();
  init_scene();
  game_loop();
}

void
Game::add_hooks() {
  _input->add_hook({OIS::KC_ESCAPE, true}, EventType::doItOnce,
                   std::bind(&Game::shutdown, this));
}

void
Game::init_scene() {
  Ogre::SceneNode* sinbad_node = _scene->create_graphic_element("sinbad_entity",
                                                  "Sinbad.mesh", "", "sinbad_node");
  sinbad_node->setPosition(3, -3 ,0);
 _scene->add_billboard(sinbad_node);
}

void
Game::shutdown() {
  _input->shutdown();
}

void
Game::game_loop() {
  _timer.start();
  while(!_input->_exit) {
    _delta += _timer.get_delta_time();
    _input->capture();
    if(_delta >= (1/FPS)) {
      _input->check_events();
      _scene->render_one_frame();
      _delta = 0.f;
    }
  }
}
