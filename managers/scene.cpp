#include "scene.h"

Scene::Scene() {
  Ogre::LogManager* logMgr = OGRE_NEW Ogre::LogManager;
  logMgr->createLog("config/ogre.log", true, false, false);
  _root = new Ogre::Root("config/plugins.cfg", "config/ogre.cfg", "");

  if (not _root->restoreConfig() )
    _root->showConfigDialog();

  _window = _root->initialise(true, "Arkanoid3D");

  load_resources();

  _scene_manager= _root->createSceneManager(Ogre::ST_GENERIC);

  create_camera(_window);

  create_light();

  _ray_query = _scene_manager->createRayQuery(Ogre::Ray());
  _ray_query->setSortByDistance(true);
}

void
Scene::load_resources() {
  Ogre::ConfigFile cf;
  cf.load("config/resources.cfg");

  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

  Ogre::String secName, typeName, archName;
  while (seci.hasMoreElements()) {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typeName = i->first;
      archName = i->second;
      Ogre::ResourceGroupManager::getSingleton()
        .addResourceLocation(archName, typeName, secName);
    }
  }

  Ogre::ResourceGroupManager::getSingleton()
    .initialiseAllResourceGroups();
}

void
Scene::render_one_frame(void) {
  Ogre::WindowEventUtilities::messagePump();
  _root->renderOneFrame();
}

Ogre::Ray
Scene::set_ray_query(float x, float y) {
  Ogre::Ray ray = _camera->
    getCameraToViewportRay(x/float(_window->getWidth()), y/float(_window->getHeight()));

  _ray_query->setRay(ray);

  return ray;
}

void
Scene::create_light() {
  _scene_manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  _scene_manager->setAmbientLight(Ogre::ColourValue(4.0, 4.0, 4.0));
}

void
Scene::create_camera(Ogre::RenderWindow* window) {
  Ogre::SceneNode* camera_node = create_node("camera_node");
  _camera = _scene_manager->createCamera("PlayerCamera");
  _camera->setPosition(Ogre::Vector3(4, -3, 19));
  _camera->setDirection(Ogre::Vector3(0, 0, 0));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(1000);

  Ogre::Viewport* viewport = window->addViewport(_camera);
  viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

  _camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()) /
                          Ogre::Real(viewport->getActualHeight()));
  camera_node->attachObject(_camera);
}

Ogre::SceneNode*
Scene::create_node(std::string name) {
  return _scene_manager->createSceneNode(name);
}

Ogre::SceneNode*
Scene::get_node(std::string node) {
  if(node == "")
    return _scene_manager->getRootSceneNode();

  if(_scene_manager->hasSceneNode(node))
    return _scene_manager->getSceneNode(node);

  return nullptr;
}

void
Scene::attach(Ogre::SceneNode* node, Ogre::Entity* entity) {
  node->attachObject(entity);
}

Ogre::SceneNode*
Scene::create_graphic_element(std::string entity, std::string mesh,
                                 std::string parent, std::string name) {
  return create_graphic_element(create_entity(entity, mesh), parent, name);
}

Ogre::SceneNode*
Scene::create_graphic_element(Ogre::Entity* entity, std::string parent, std::string name) {
  Ogre::SceneNode* node = get_child_node(parent, name);
  attach(node, entity);

  return node;
}

Ogre::SceneNode*
Scene::create_plane(std::string axis, std::string name, std::string mesh,
        std::string parent, std::string material) {
  Ogre::Plane planeGround(get_axis(axis), -3);
  Ogre::MeshManager::getSingleton().createPlane(mesh,
  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, planeGround,
  200,200,1,1,true,1,20,20,get_normal(axis));

  Ogre::SceneNode* ground_node = get_child_node(parent, name);
  Ogre::Entity* ground_entity = create_entity(name, mesh);

  ground_entity->setMaterialName(material);
  ground_node->attachObject(ground_entity);

  return ground_node;
}

Ogre::SceneNode*
Scene::get_child_node(Ogre::SceneNode* parent, std::string name) {
  Ogre::SceneNode* node = get_node(name);
  if(node == nullptr)
      node = create_child_node(parent, name);

  return node;
}

Ogre::SceneNode*
Scene::get_child_node(std::string parent, std::string name) {
  return get_child_node(get_node(parent), name);
}

Ogre::SceneNode*
Scene::create_child_node(Ogre::SceneNode* parent, std::string name) {
  Ogre::SceneNode* node = create_node(name);
  add_child(parent, node);

  return node;
}

Ogre::Entity*
Scene::create_entity(std::string name, std::string mesh) {
  return _scene_manager->createEntity(name, mesh);
}

void
Scene::add_child(Ogre::SceneNode* parent, Ogre::SceneNode* child) {
  parent->addChild(child);
}

void
Scene::move_node(std::string node_name, Ogre::Vector3 increment) {
  Ogre::SceneNode* node = get_node(node_name);

  Ogre::Vector3 position = node->getPosition();

  node->setPosition(position + increment);
}

Ogre::ParticleSystem*
Scene::get_particle(std::string name, std::string particle_system) {
  return get_particle(get_node(name), name, particle_system);
}

Ogre::ParticleSystem*
Scene::get_particle(Ogre::SceneNode* node, std::string name, std::string particle_system) {
  if(_scene_manager->hasParticleSystem(name))
    return _scene_manager->getParticleSystem(name);

  Ogre::ParticleSystem* particle =_scene_manager->createParticleSystem(name, particle_system);

  node->attachObject(particle);

  return particle;
}

Ogre::ParticleSystem*
Scene::get_particle(std::string name, std::string particle_system, Ogre::Vector3 position) {
  Ogre::SceneNode* node = get_child_node("", name);
  node->setPosition(position);

  return get_particle(node, name, particle_system);
}

void
Scene::destroy_node(std::string name) {
  destroy_node(get_node(name));
}

void
Scene::destroy_node(Ogre::SceneNode* child) {
  destroy_all_attached_movable_objects(child);
  _scene_manager->destroySceneNode(child);
}

void
Scene::remove_child(std::string parent, std::string child) {
  remove_child(get_node(parent), get_node(child));
}

void
Scene::remove_child(std::string parent, Ogre::SceneNode* child) {
  remove_child(get_node(parent), child);
}

void
Scene::remove_child(Ogre::SceneNode* parent, std::string child) {
  remove_child(parent, get_node(child));
}

void
Scene::remove_child(Ogre::SceneNode* parent, Ogre::SceneNode* child) {
  parent->removeChild(child);
}

void
Scene::destroy_scene() {
  destroy_all_attached_movable_objects(get_node(""));
  get_node("")->removeAndDestroyAllChildren();
}

void
Scene::destroy_all_attached_movable_objects(Ogre::SceneNode* node) {
    for(auto object: node->getAttachedObjectIterator())
       _scene_manager->destroyMovableObject(object.second);

    for(auto child: node->getChildIterator())
        destroy_all_attached_movable_objects(static_cast<Ogre::SceneNode*>(child.second));
}

Ogre::Vector3
Scene::get_axis(std::string axis){
  if(axis == "X")
    return Ogre::Vector3::UNIT_X;

  if(axis == "Y")
    return Ogre::Vector3::UNIT_Y;

  if(axis == "Z")
    return Ogre::Vector3::UNIT_Z;

  return Ogre::Vector3::UNIT_X;
}

Ogre::Vector3
Scene::get_normal(std::string axis) {
  if(axis == "X")
    return Ogre::Vector3::UNIT_Z;

  if(axis == "Y")
    return Ogre::Vector3::UNIT_X;

  if(axis == "Z")
    return Ogre::Vector3::UNIT_Y;

  return Ogre::Vector3::UNIT_X;
}

void
Scene::add_billboard(Ogre::SceneNode* node) {
  Ogre::BillboardSet* nick_billboardset =
    _scene_manager->createBillboardSet("nick_billboardset", 1);

  nick_billboardset->setBillboardType(Ogre::BBT_POINT);
  nick_billboardset->setMaterialName("SinbadName");
  nick_billboardset->setDefaultDimensions(4.,1.5);
  nick_billboardset->createBillboard(Ogre::Vector3(0,0,0));
  Ogre::SceneNode* child_node = get_child_node(node, node->getName() + "Billboard");
  child_node->setPosition(0,5.5,0);
  child_node->attachObject(nick_billboardset);

}
