#ifndef INPUT_HPP
#define INPUT_HPP
#include <map>
#include <vector>
#include <memory>
#include <unistd.h>

#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

enum class EventType{repeat, doItOnce};
class EventListener: public Ogre::WindowEventListener,
  public OIS::KeyListener,
  public OIS::MouseListener {

  typedef bool OnKeyPressed;
  typedef std::pair<OIS::MouseButtonID, OnKeyPressed> MouseKey;
  typedef std::pair<OIS::KeyCode, OnKeyPressed> KeyBoardKey;

  OIS::InputManager* _input_manager;
  OIS::Mouse* _mouse;
  OIS::Keyboard* _keyboard;

  MouseKey _mouse_key;

  std::map<MouseKey, std::function<void()>> _mouse_triggers;

 public:
  typedef std::shared_ptr<EventListener> shared;
  typedef std::vector<KeyBoardKey> KeyEvents;

  float _x, _y;
  bool _exit;

  EventListener(Ogre::RenderWindow* window);

  void capture(void);
  void check_events();

  void add_hook(MouseKey key,std::function<void()> callback);
  void add_hook(KeyBoardKey keystroke, EventType type, std::function<void()> callback);
  void clear_hooks();

  bool keyPressed(const OIS::KeyEvent& arg);
  bool keyReleased(const OIS::KeyEvent& arg);
  bool mouseMoved(const OIS::MouseEvent&  evt);
  bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
  bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id);

  void windowClosed(Ogre::RenderWindow* window);

  bool shutdown(void);

 private:
  KeyEvents _events;
  std::map<KeyBoardKey, std::function<void()>> _repeat_triggers, _doitonce_triggers;

  void create_input_manager(Ogre::RenderWindow* window);
  void remove_key_from_buffer(KeyBoardKey event);

  void trigger_keyboard_events();
  void trigger_mouse_events();
  };
#endif
